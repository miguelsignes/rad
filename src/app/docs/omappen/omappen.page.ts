import { Component, OnInit } from '@angular/core';
import { Article } from './../../interfaces/interfaces';
import { map } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';


@Component({
  selector: 'app-omappen',
  templateUrl: './omappen.page.html',
  styleUrls: ['./omappen.page.scss'],
})


export class OmappenPage implements OnInit {

  private cardCollection: AngularFirestoreCollection<Article>;
  imgSrc:String = '../../../assets/images/paris.jpg';
  data: any = [];

  
  constructor(private afs:AngularFirestore) {
 this.cardCollection = this.afs.collection<Article>('articulos/', ref=>
    ref.where('categoria', '==', 'Dokumentasjon').limit(1)  
 );
   }

  ngOnInit() {
    this.cargarDoc();
  }

  
cargarDoc() {

   this.cardCollection.snapshotChanges().pipe(
      map(actions => actions.map( a=> {
        const data = a.payload.doc.data() as Article;
        const id = a.payload.doc.id;
    
        return { id, ...data}
      }))
    ).subscribe( (data) => {

        console.log(data[0]);
        this.data = data[0];
        console.log(this.data);
    })
  }

}
